<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>STIE &amp; STMIK JAYAKARTA</title>
    <link rel="icon" href="http://jayakarta.ac.id/wp-content/uploads/2020/04/cropped-icon-192x192.png" sizes="192x192" />
  </head>
 
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">

      <a class="navbar-brand" href="#"><img src="http://jayakarta.ac.id/wp-content/uploads/2015/07/cropped-logo-jayakarta.png" height="50" class="d-inline-block align-top" alt="" loading="lazy"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/pendaftaran/login" class="btn btn-outline-success my-2 my-sm-0">Login Calon Mahasiswa</a>
        </form>
      </div>
    </div>
  </nav>

  <body>