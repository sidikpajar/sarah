<?php 
  include('login/db_connect.php');
  include('components/header.php'); 
?>
    <div class="container" style="margin-top:50px">
      <h1>Pendaftaran Online</h1>
      <form role="form" method="POST" action="index.php" enctype="multipart/form-data">
        <div class="form-group" >
          <label for="exampleInputPassword1">Nama Lengkap</label>
          <input type="text" name="name" class="form-control" placeholder="M Fajar Sidik">
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group" >
              <label for="exampleInputPassword1">Jenis Kelamin</label>
              <select name="gender" class="form-control" id="exampleFormControlSelect1">
                <option value="Laki-laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group" >
              <label for="exampleInputPassword1">Status Pernikahan</label>
              <select name="maritial_status" class="form-control" id="exampleFormControlSelect1">
                <option value="Belum Kawin">Belum Kawin</option>
                <option value="Kawin">Kawin</option>
                <option value="Cerai Hidup">Cerai Hidup</option>
                <option value="Cerai Mati">Cerai Mati</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group" >
              <label for="exampleInputPassword1">Agama</label>
              <select name="religion" class="form-control" id="exampleFormControlSelect1">
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Katolik">Katolik</option>
                <option value="Hindu">Hindu</option>
                <option value="Buddha">Buddha</option>
                <option value="Kong Hu Cu">Kong Hu Cu</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group" >
              <label for="exampleInputPassword1">Tanggal Lahir</label>
              <input type="date" name="birth_date" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group" >
              <label for="exampleInputPassword1">Tempat Lahir</label>
              <input type="text" name="place_of_birth" class="form-control" placeholder="Jakarta">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Username</label>
          <input type="text" name="user_name" class="form-control"  placeholder="hellofajars">
          <small id="emailHelp" class="form-text text-muted">Digunakan untuk melakukan login kedalam aplikasi</small>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Password</label>
          <input type="text" name="user_password" class="form-control"  placeholder="********">
          <small id="emailHelp" class="form-text text-muted">Digunakan untuk melakukan login kedalam aplikasi</small>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Password Confirmation</label>
          <input type="text" class="form-control"  placeholder="********">
          <small id="emailHelp" class="form-text text-muted">Digunakan untuk melakukan login kedalam aplikasi</small>
        </div>
        <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
        <?php
        if(isset($_POST["submit"])) {
          $user_id        = rand(1000000000,9999999999);
          $user_level     = 'mahasiswa';
          $user_password  = $_POST['user_password'];
          $user_name      = $_POST['user_name'];
          $name           = $_POST['name'];
          $gender         = $_POST['gender'];
          $maritial_status= $_POST['maritial_status'];
          $religion       = $_POST['religion'];
          $birth_date     = $_POST['birth_date'];
          $place_of_birth = $_POST['place_of_birth'];
          $status         = 'ACTIVE';

          $sql  = "INSERT INTO users (user_id, 
            user_level, 
            user_password,
            user_name,
            name,
            gender,
            maritial_status,
            religion,
            birth_date,
            place_of_birth,
            status
            ) VALUES (
              '$user_id',
              '$user_level',
              '$user_password',
              '$user_name',
              '$name',
              '$gender',
              '$maritial_status',
              '$religion',
              '$birth_date',
              '$place_of_birth',
              '$status'
              )";
              if ($connect-> query($sql) === TRUE ) {
                echo "
                <script type='text/javascript'>
                    alert('Users ".$name." Berhasil ditambah');
                    window.location = 'login';
                </script>";
              } else {
                  echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
              }
            $connect->close();
          }
        ?>
      </form>
      
    </div>
<?php include('components/footer.php'); ?>