<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-6 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ubah Jurusan</h3>
                  </div>

                  <?php
                    $jurusan_id = $_GET['jurusan_id'];
                    $show_kontak = mysqli_query($connect,"SELECT * FROM jurusan WHERE jurusan_id='$jurusan_id' ");
                    while($row = mysqli_fetch_array($show_kontak)) {
                  ?>

                  <form role="form" method="POST" action="data-master-jurusan-ubah.php" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Jurusan</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $row['nama']?>" required>
                      </div>

                      <label for="exampleInputEmail1">Deskripsi</label>
                      <textarea id="example" id="deskripsi" name="deskripsi">
                        <div class="fr-view">
                          <?php echo $row['deskripsi']?>
                        </div>
                      </textarea>
                      <input type="hidden" class="form-control" id="jurusan_id" name="jurusan_id" value="<?php echo $row['jurusan_id']?>" required>
                    </div>
                    <div class="box-footer">
                      <a href="data-master-jurusan.php" class="btn btn-primary">Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                    <?php } ?>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $jurusan_id     = $_POST['jurusan_id'];
                      $nama           = $_POST['nama'];
                      $deskripsi      = $_POST['deskripsi'];
                      $sql = "UPDATE jurusan SET
                              nama='$nama',
                              deskripsi='$deskripsi'
                              WHERE jurusan_id = '$jurusan_id' ";
                    
                      
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Jurusan ".$nama." Berhasil diubah');
                            window.location = 'data-master-jurusan.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  
  <?php
  include("component/footer.php");
   ?>
