<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Master - Program Studi
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Data Master</a></li>
        <li class="active">Program Studi</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <div style="padding-top:20px">
                <a class="btn btn-primary" href="data-master-program-studi-tambah.php" >Relasikan Program Studi</a>
              </div>
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row" style="background-color:#00a65a; color: white">
                          <th>Studi ID</th>
                          <th>FAKULTAS ID</th>
                          <th>JURUSAN ID</th>
                          <th>FAKULTAS</th>
                          <th>JURUSAN</th>
                          <th>KONFIGURASI</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT 	
                                  program_studi.studi_id AS STUDIID, 
                                  program_studi.fakultas_id AS FAKULTASID,
                                    program_studi.jurusan_id AS JURUSANID,
                                    f.nama AS NAMAFAKULTAS,
                                    j.nama AS NAMAJURUSAN
                                FROM program_studi
                                INNER JOIN jurusan j
                                  ON j.jurusan_id = program_studi.jurusan_id
                                INNER JOIN fakultas f
                                  ON f.fakultas_id = program_studi.fakultas_id";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                            ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $row['STUDIID'] ?></td>
                            <td ><?php echo $row['FAKULTASID'] ?></td>
                            <td ><?php echo $row['JURUSANID'] ?></td>
                            <td ><?php echo $row['NAMAFAKULTAS'] ?></td>
                            <td ><?php echo $row['NAMAJURUSAN'] ?></td>
                            <td>
                              <?php
                                echo "<a onClick='myFunction(".$row['STUDIID'].")' style='margin:5px; padding:5px;' href='#' class='btn btn-xs btn-danger'>HAPUS</a>";
                                  
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>

  <script>
    function myFunction(jurusan_id) {
      console.log(jurusan_id)
      var r = confirm(`Anda yakin ingin menghapus Program Studi id ${jurusan_id}`);
      if (r == true) {
        window.location = 'data-master-program-studi-hapus?studi_id=' + jurusan_id;
      } else {
        
      }
    }
  </script>
  <?php
  include("component/footer.php");
   ?>
