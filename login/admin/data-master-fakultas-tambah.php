<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-6 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Fakultas</h3>
                  </div>
                  <form role="form" method="POST" action="data-master-fakultas-tambah.php" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Fakultas</label>
                        <input type="text" class="form-control" id="nama" name="nama" required>
                      </div>
                
                    </div>
                    <div class="box-footer">
                      <a href="data-master-fakultas.php" class="btn btn-primary">Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $fakultas_id     = rand(1000000000,9999999999);
                      $nama           = $_POST['nama'];
                      $sql            = "INSERT INTO fakultas (fakultas_id, nama) VALUES ('$fakultas_id','$nama')";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('fakultas ".$nama." Berhasil ditambah');
                            window.location = 'data-master-fakultas.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  
  <?php
  include("component/footer.php");
   ?>
