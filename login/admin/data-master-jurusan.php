<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Master - Jurusan
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Data Master</a></li>
        <li class="active">Jurusan</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <div style="padding-top:20px">
                <a class="btn btn-primary" href="data-master-jurusan-tambah.php" >Tambah Jurusan</a>
              </div>
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row" style="background-color:#00a65a; color: white">
                          <th>JURUSAN ID</th>
                          <th>JURUSAN</th>
                          <th>KONFIGURASI</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT * FROM jurusan";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                              $jurusan_id = $row['jurusan_id'] ;
                              $nama = $row['nama']
                            ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $jurusan_id ?></td>
                            <td ><?php echo $nama ?></td>
                            
                            <td>
                              <?php
                                  echo "<a style='margin:5px; padding:5px;' href='data-master-jurusan-ubah.php?jurusan_id=".$jurusan_id."' class='btn btn-xs btn-warning'>UBAH</a>";
                                  echo "<a onClick='myFunction(".$jurusan_id.")' style='margin:5px; padding:5px;' href='#' class='btn btn-xs btn-danger'>HAPUS</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>

  <script>
    function myFunction(jurusan_id) {
      console.log(jurusan_id)
      var r = confirm(`Anda yakin ingin menghapus jurusan id ${jurusan_id}`);
      if (r == true) {
        window.location = 'data-master-jurusan-hapus.php?jurusan_id=' + jurusan_id;
      } else {
        
      }
    }
  </script>

  <?php
  include("component/footer.php");
   ?>
