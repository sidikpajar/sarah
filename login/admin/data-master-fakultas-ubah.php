<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-6 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ubah Fakultas</h3>
                  </div>

                  <?php
                    $fakultas_id = $_GET['fakultas_id'];
                    $show_kontak = mysqli_query($connect,"SELECT * FROM fakultas WHERE fakultas_id='$fakultas_id' ");
                    while($row = mysqli_fetch_array($show_kontak)) {
                  ?>

                  <form role="form" method="POST" action="data-master-fakultas-ubah.php" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Fakultas</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $row['nama']?>" required>
                      </div>
                      <input type="hidden" class="form-control" id="fakultas_id" name="fakultas_id" value="<?php echo $row['fakultas_id']?>" required>
                    </div>
                    <div class="box-footer">
                      <a href="data-master-fakultas.php" class="btn btn-primary">Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                    <?php } ?>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $fakultas_id     = $_POST['fakultas_id'];
                      $nama           = $_POST['nama'];
                      $sql = "UPDATE fakultas SET
                              nama='$nama'
                              WHERE fakultas_id = '$fakultas_id' ";
                    
                      
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Fakultas ".$nama." Berhasil diubah');
                            window.location = 'data-master-fakultas.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  
  <?php
  include("component/footer.php");
   ?>
