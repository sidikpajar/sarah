<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data List Pendaftar
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>Data List</a></li>
        <li class="active">Pendaftar</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class=box-header>
              <a class='btn btn-success' href="printData.php">Go to preview data download</a>
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row" style="background-color:#00a65a; color: white">
                          <th>Registrasi ID</th>
                          <th>NIM / User ID</th>
                          <th>Nama</th>
                          <th>Kategori</th>
                          <th>Status</th>
                          <th>Tanggal Daftar</th>
                          <th>Tanggal Update</th>
                          <th>AKSI</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT 
                                      r.id AS REGISTERID, 
                                      r.user_id AS USERID,
                                      r.id_studi AS PROGRAMSTUDI,
                                      r.category AS CATEGORY,
                                      r.status AS STATUS,
                                      r.create_at AS TANGGALMENDAFTAR,
                                      r.modified_at AS TANGGALUPDATE,
                                      u.name AS NAMAPENDAFTAR
                                  FROM regis_ppdb r
                                    INNER JOIN users u 
                                        ON u.user_id = r.user_id
                                  ";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                              $registrasi_id = $row['REGISTERID'];
                              $user_id = $row['USERID'];
                              $program_studi = $row['PROGRAMSTUDI'];
                            ?>
                          <tr role="row" class="odd">
                            <td><?php echo $row['REGISTERID']; ?></td>
                            <td><?php echo $row['USERID']; ?></td>
                            <td><?php echo $row['NAMAPENDAFTAR']; ?></td>
                            <td><?php echo $row['CATEGORY']; ?></td>
                            <td><?php 
                                  if($row['STATUS'] == 'Diterima'){
                                    echo "<span class='btn btn-xs btn-success'>Diterima</span>";
                                    echo "<a href='../../sms.php' style='margin-left:10px;' class='btn btn-xs btn-info'>Kirim SMS Diterima</a>";
                                  } else if($row['STATUS'] == 'Ditolak'){
                                    echo "<span class='btn btn-xs btn-danger'>Ditolak</span>";
                                  } else if($row['STATUS'] == 'Registrasi'){
                                    echo "<span class='btn btn-xs btn-primary'>Registrasi</span>";
                                  } else if($row['STATUS'] == 'Pemeriksaan pembayaran & berkas oleh Admin'){
                                    echo "<span class='btn btn-xs btn-info'>Pemeriksaan pembayaran & berkas oleh Admin</span>";
                                  }
                                ?>
                            </td>
                            <td><?php echo $row['TANGGALMENDAFTAR']; ?></td>
                            <td><?php echo $row['TANGGALUPDATE']; ?></td>
                            <td>
                              <?php
                                  echo "<a style='margin:5px; padding:5px;' href='data-pendaftar.php?registrasi_id=".$registrasi_id."&user_id=".$user_id."&program_studi=".$program_studi."  ' class='btn btn-xs btn-warning'>Detail</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>



  <?php
  include("component/footer.php");
   ?>
