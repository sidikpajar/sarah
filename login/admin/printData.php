<?php
  date_default_timezone_set('Asia/Jakarta');
  include('../db_connect.php');
  include('cekadmin.php');
?>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  padding:10px;
}
a{
  margin: 20px;
}
@media print{@page {size: landscape}}
</style>
<style type="text/css" media="print">
    .page
    {
     -webkit-transform: rotate(-90deg); 
     -moz-transform:rotate(-90deg);
     filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
</style>
<div style="padding:10px;">
  <a href="index.php">Kembali</a>
  <a onClick='dataPrint()' href="#">Download</a>
</div>

<table>
  <tr>
    <th colspan="8">Data Registrasi</th>
    <th colspan="6">DATA MAHASISWA</th>
  </tr>
  <tr>
    <th>USER ID / NIM</th>
    <th>REGISTRASI ID</th>
    <th>NAMA FAKULTAS</th>
    <th>NAMA JURUSAN</th>
    <th>KATEGORI PENDAFTARAN</th>
    <th>TANGGAL MENDAFTAR</th>
    <th>TANGGAL STATUS TERUPDATE</th>
    <th>STATUS REGISTRASI</th>
    <th>NAMA PENDAFTAR</th>
    <th>JENIS KELAMIN</th>
    <th>STATUS PERNIKAHAN</th>
    <th>AGAMA</th>
    <th>TANGGAL LAHIR</th>
    <th>TEMPAT LAHIR</th>
  </tr>
  <tbody>
    <?php
      $sql="SELECT 
      r.user_id AS USERID,
      r.id AS REGISTER_ID,
      r.status AS REGISTER_STATUS, 
      f.nama AS NAMA_FAKULTAS,
      j.nama AS NAMA_JURUSAN,
      r.category AS CATEGORY,
      r.status AS STATUS,
      r.create_at AS TANGGAL_MENDAFTAR,
      r.modified_at AS TANGGAL_UPDATE,
      u.name AS NAMA_PENDAFTAR,
      u.gender AS JENIS_KELAMIN,
      u.maritial_status AS STATUS_PERNIKAHAN,
      u.religion AS AGAMA,
      u.birth_date AS TANGGAL_LAHIR,
      u.place_of_birth AS TEMPAT_LAHIR
  FROM regis_ppdb r
    INNER JOIN program_studi ps 
          ON ps.studi_id = r.id_studi
      INNER JOIN users u 
         ON u.user_id = r.user_id
      INNER JOIN fakultas f
        ON f.fakultas_id = ps.fakultas_id
      INNER JOIN jurusan j
        ON j.jurusan_id = ps.jurusan_id";
      $query = mysqli_query($connect,$sql);
      while($row = mysqli_fetch_array($query)) {
      ?>
    <tr>
      <td><?php echo $row['USERID']; ?></td>
      <td><?php echo $row['REGISTER_ID']; ?></td>
      <td><?php echo $row['NAMA_FAKULTAS']; ?></td>
      <td><?php echo $row['NAMA_JURUSAN']; ?></td>
      <td><?php echo $row['CATEGORY']; ?></td>
      <td><?php echo $row['TANGGAL_MENDAFTAR']; ?></td>
      <td><?php echo $row['TANGGAL_UPDATE']; ?></td>
      <td><?php 
            if($row['STATUS'] == 'Diterima'){
              echo "<span class='btn btn-xs btn-success'>Diterima</span>";
            } else if($row['STATUS'] == 'Ditolak'){
              echo "<span class='btn btn-xs btn-danger'>Ditolak</span>";
            } else if($row['STATUS'] == 'Registrasi'){
              echo "<span class='btn btn-xs btn-primary'>Registrasi</span>";
            } else if($row['STATUS'] == 'Pemeriksaan pembayaran & berkas oleh Admin'){
              echo "<span class='btn btn-xs btn-info'>Pemeriksaan pembayaran & berkas oleh Admin</span>";
            }
          ?>
      </td>
      <td><?php echo $row['NAMA_PENDAFTAR']; ?></td>
      <td><?php echo $row['JENIS_KELAMIN']; ?></td>
      <td><?php echo $row['STATUS_PERNIKAHAN']; ?></td>
      <td><?php echo $row['AGAMA']; ?></td>
      <td><?php echo $row['TANGGAL_LAHIR']; ?></td>
      <td><?php echo $row['TEMPAT_LAHIR']; ?></td>
    </tr>
    <?php
      }
      ?>
  </tbody>
</table>


<script>
    function dataPrint() {
      window.print(); 
    }
  </script>