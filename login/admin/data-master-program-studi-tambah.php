<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-6 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Program Studi</h3>
                  </div>
                  <form role="form" method="POST" action="data-master-program-studi-tambah.php" enctype="multipart/form-data">
                    <div class="box-body">
                      
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Pilih Fakultas</label>
                      <select name="fakultas_id" class="form-control" id="exampleFormControlSelect1">
                      <?php
                        //ambil data dari table program studi
                        $fakultas="SELECT * from fakultas";
                        $queryFakultas = mysqli_query($connect,$fakultas);
                        //tampilkan data-data hasil query
                        while($row = mysqli_fetch_array($queryFakultas)) {
                      ?>
                        <option value="<?php echo $row['fakultas_id']; ?>"><?php echo ''.$row['nama'].''; ?></option>
                      <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Pilih Jurusan</label>
                      <select name="jurusan_id" class="form-control" id="exampleFormControlSelect1">
                      <?php
                        //ambil data dari table program studi
                        $jurusan="SELECT * from jurusan";
                        $queryJurusan = mysqli_query($connect,$jurusan);
                        //tampilkan data-data hasil query
                        while($row = mysqli_fetch_array($queryJurusan)) {
                      ?>
                        <option value="<?php echo $row['jurusan_id']; ?>"><?php echo ''.$row['nama'].''; ?></option>
                      <?php } ?>
                      </select>
                    </div>

                    </div>
                    <div class="box-footer">
                      <a href="data-master-program-studi.php" class="btn btn-primary">Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $studi_id     = rand(10000000,99999999);
                      $jurusan_id   = $_POST['jurusan_id'];
                      $fakultas_id  = $_POST['fakultas_id'];
                      $sql          = "INSERT INTO program_studi (studi_id, fakultas_id, jurusan_id) VALUES ('$studi_id','$fakultas_id','$jurusan_id')";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Program Studi Berhasil ditambah');
                            window.location = 'data-master-program-studi.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  
  <?php
  include("component/footer.php");
   ?>
