

<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <!-- Untuk menampilkan Pengumuman -->
    <section class="content-header">

    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Status Pendaftaran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Regostrasi ID</th>
                    <th>Kategory Registrasi</th>
                    <th>Tanggal Registrasi</th>
                    <th>Status Registrasi</th>
                    <th>Detail</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $user_id  = $_SESSION['user_id'];
                    $brt="SELECT * FROM regis_ppdb WHERE user_id = '$user_id'";
                    $query = mysqli_query($connect,$brt);
                    while($row = mysqli_fetch_array($query)) {
                       $id_studi = $row['id_studi'];
                       $registrasi_id = $row['id'];
                  ?>
                  <tr role="row" class="odd">
                    <td><?php echo $row['id'] ?></td>
                    <td><?php echo $row['category'] ?></td>
                    <td><?php echo $row['modified_at'] ?></td>
                    <td><?php 
                          if($row['status'] === 'Registrasi'){
                            echo "Telah melakukan registrasi, silakan melakukan pembayaran disini <a href='pembayaran.php?id=".$row['id']."' >Pembayaran</a>";
                          } else {
                            echo $row['status'];
                          }
                        ?>
                    </td>
                    <td>
                      <?php
                          echo "<a href='pendaftaran-detail.php?id=".$row['id']."' class='btn btn-xs btn-primary'>Detail</a>";
                      ?>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        

        

      </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
  ?>
