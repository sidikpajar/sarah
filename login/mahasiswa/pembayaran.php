<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Pembayaran</h3>
                  </div>
                  <form role="form" method="POST" action="pembayaran.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Transfer</label>
                        <input type="date" class="form-control" id="tanggal_transfer" name="tanggal_transfer" required>
                        <input type="hidden" class="form-control" id="registrasi_id" name="registrasi_id" value="<?php echo $_GET['id']; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Atas Nama</label>
                        <input type="text" class="form-control" id="atas_nama" name="atas_nama" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nominal Transfer</label>
                        <input type="number" class="form-control" id="nominal" name="nominal" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nomor Rekening</label>
                        <input type="number" class="form-control" id="nomor_rek" name="nomor_rek" required>
                      </div>
                      
                      <div class="form-group">
                        <label for="exampleInputEmail1">Bukti transfer</label>
                        <input type="file" name="image" required>
                      </div>

                    </div>
                    <div class="box-footer">
                      <a href="status-pendaftaran.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                        if(isset($_POST["submit"])){
                          $check = $_FILES["image"]["tmp_name"];
                          if($check !== false){
                              $id             = rand(1111,99999);
                              $user_id        = $_SESSION['user_id'];
                              $registrasi_id  = $_POST['registrasi_id'];
                              $tanggal        = $_POST['tanggal_transfer'];
                              $atas_nama      = $_POST['atas_nama'];
                              $nomor_rek      = $_POST['nomor_rek'];
                              $nominal        = $_POST['nominal'];
                              
                              $lokasi_file    = $_FILES['image']['tmp_name'];
                              $nama_file      = $_FILES['image']['name'];
                              $folder         = "../files-bukti-pembayaran/$id+$nama_file";
                              $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                              $modified_at    = (new DateTime('now'))->format('Y-m-d H:i:s');
                              $a = "$id+$nama_file";
                              if (move_uploaded_file($lokasi_file,"$folder")){
                                echo "Nama File : <b>$nama_file</b> sukses di upload";
                              
                              $brt = "INSERT INTO pembayaran (id, user_id, registrasi_id, tanggal_transfer, atas_nama, nomor_rek, nominal, image, create_at, modified_at)
                              VALUES ('$id','$user_id','$registrasi_id','$tanggal','$atas_nama', '$nomor_rek', '$nominal', '$a', '$create_at', '$modified_at')
                              ";

                                if ($connect-> query($brt) === TRUE) {
                                     echo "
                                    <script type= 'text/javascript'>
                                        alert('sukses melakukan pembayaran');
                                        window.location = 'pembayaran-status-update.php?registrasi_id={$registrasi_id}';
                                    </script>";
                                  } else {
                                        echo "<script type= 'text/javascript'>alert('Error: " . $brt . "<br>" . $connect->error."');</script>";
                                      }
                                }else{
                                    echo "<script type= 'text/javascript'>alert('File upload failed, please try again');</script>";
                                }
                              }
                              else{
                                echo "File gagal di upload";
                              }
                          }else{
                              $connect->close();
                          }
                      ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
