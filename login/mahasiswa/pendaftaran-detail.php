<?php
  include("component/header.php");
  include("component/sidebar.php");
  $user_id = $_SESSION['user_id'];
?>
<div class="content-wrapper">
	<section class="content-header">
	<h1>Data Pribadi </h1> 
	<ol class="breadcrumb">
		<li>
			<a href="index.php"><i class="fa fa-dashboard"></i> Data Pribadi</a>
		</li>
		<li class="active">Data Pribadi</li>
	</ol>
	</section>
	<section class="content">
    <div class="row">
      <?php
        
        $personalMe="SELECT * FROM users WHERE user_id='$user_id' ";
        $queryPersonalMe = mysqli_query( $connect, $personalMe );
        while($row = mysqli_fetch_array( $queryPersonalMe )) {
      ?>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                 Data Pribadi
              </h3>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>NIM <span class='pull-right'> <?php echo $row['user_id'];?></span></a></li>
                <li><a href='#'>Nama Lengkap <span class='pull-right'> <?php echo $row['name'];?></span></a></li>
                <li><a href='#'>Username <span class='pull-right'> <?php echo $row['user_name'];?></span></a></li>
                <li><a href='#'>Jenis Kelamin <span class='pull-right'> <?php echo $row['gender'];?></span></a></li>
                <li><a href='#'>Status Pernikahan <span class='pull-right'> <?php echo $row['maritial_status'];?></span></a></li>
                <li><a href='#'>Agama <span class='pull-right'> <?php echo $row['religion'];?></span></a></li>
                <li><a href='#'>Tanggal Lahir <span class='pull-right'> <?php echo $row['birth_date'];?></span></a></li>
                <li><a href='#'>Kota lahir <span class='pull-right'> <?php echo $row['place_of_birth'];?></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      <?php } ?>
      <?php 
        $id = $_GET['id'];
        $regisPPDB="SELECT * FROM regis_ppdb WHERE id='$id' ";
        $query = mysqli_query( $connect, $regisPPDB );
        while($row = mysqli_fetch_array( $query )) {
          $program_studi = $row['id_studi']
        ?>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                 Data Registration
              </h3>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>Registraion ID <span class='pull-right'> <?php echo $row['id'];?></span></a></li>
                <li><a href='#'>Program Studi 
                <span class='pull-right'>
                  <?php 
                    $sql2="SELECT 	
                        program_studi.studi_id AS STUDIID, 
                        program_studi.fakultas_id AS FAKULTASID,
                          program_studi.jurusan_id AS JURUSANID,
                          f.nama AS NAMAFAKULTAS,
                          j.nama AS NAMAJURUSAN
                      FROM program_studi
                      INNER JOIN jurusan j
                        ON j.jurusan_id = program_studi.jurusan_id
                      INNER JOIN fakultas f
                        ON f.fakultas_id = program_studi.fakultas_id
                      WHERE program_studi.studi_id = {$program_studi}";
                      $query2 = mysqli_query($connect,$sql2);
                      while($rowList = mysqli_fetch_array($query2)) {
                        echo ''.$rowList['NAMAFAKULTAS'].' - '.$rowList['NAMAJURUSAN'].'';
                      }
                    ?>
                </span></a></li>
                <li><a href='#'>Kategory <span class='pull-right'><?php echo $row['category'];?></span></a></li>
                <li><a href='#'>Tanggal Modifikasi <span class='pull-right'> <?php echo $row['modified_at'];?></span></a></li>
                <li><a href='#'>Status Registrasi <span class='pull-right'> <?php echo $row['status'];?></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      <?php } ?>
      <?php 
        $berkas="SELECT * FROM berkas WHERE user_id='$user_id' ";
        $queryberkas = mysqli_query( $connect, $berkas );
        while($rowberkas = mysqli_fetch_array( $queryberkas )) {
        ?>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                 Berkas Registration
              </h3>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='../files/<?php echo $rowberkas['ijazah']; ?>' download>Ijazah <span class='pull-right' style='color:#3c8dbc'>Download here</span></a></li>
                <li><a href='../files/<?php echo $rowberkas['skhun'];?>' download>SKHUN <span class='pull-right' style='color:#3c8dbc'>Download here</span></a></li>
                <li><a href='../files/<?php echo $rowberkas['ktp'];?>' download>KTP <span class='pull-right' style='color:#3c8dbc'> Download here</span></a></li>
                <li><a href='../files/<?php echo $rowberkas['kk'];?>' download>KK <span class='pull-right' style='color:#3c8dbc'> Download here</span></a></li>
                <li><a href='../files/<?php echo $rowberkas['foto2x3'];?>' download>Foto 2X3 <span class='pull-right' style='color:#3c8dbc'> Download here</span></a></li>
                <li><a href='../files/<?php echo $rowberkas['foto3x4'];?>' download>Foto 3x4 <span class='pull-right' style='color:#3c8dbc'> Download here</span></a></li>
                <?php 
                  if($rowberkas['transkrip'] !== ''){
                    ?>
                      <li><a href='../files/<?php echo $rowberkas['ijazah']; ?>' download>Transkrip <span class='pull-right' style='color:#3c8dbc'>Download here</span></a></li>
                    <?php
                  } else {
                    ?>
                      <li><a href='#'>Transkrip <span class='pull-right'>Belum melakukan upload</span></a></li>
                    <?php
                  }
                ?>
              </ul>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row" style="background-color:#00a65a; color: white">
                          <th>Nomor Pembayaran</th>
                          <th>Tanggal Transfer</th>
                          <th>Nomor Rekening</th>
                          <th>Atas Nama</th>
                          <th>Nominal</th>
                          <th>Tanggal Submit Pembayaran</th>
                          <th>Bukti pembayaran</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $pembayaran="SELECT * FROM pembayaran WHERE user_id='$user_id' AND registrasi_id='$id' ";
                            $querypembayaran = mysqli_query( $connect, $pembayaran );
                            while($rowPembayaran = mysqli_fetch_array( $querypembayaran )) {
                          ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $rowPembayaran['id'] ?></td>
                            <td ><?php echo $rowPembayaran['tanggal_transfer'] ?></td>
                            <td ><?php echo $rowPembayaran['nomor_rek'] ?></td>
                            <td ><?php echo $rowPembayaran['atas_nama'] ?></td>
                            <td ><?php echo $rowPembayaran['nominal'] ?></td>
                            <td ><?php echo $rowPembayaran['create_at'] ?></td>
                            <td ><a href='../files-bukti-pembayaran/<?php echo $rowPembayaran['image']; ?>' download>Download here</a></td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
    </div>
	</section>
</div>
</div>
<script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
<?php
  include("component/footer.php");
   ?>