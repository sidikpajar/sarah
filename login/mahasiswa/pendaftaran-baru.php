<?php
  include("component/header.php");
  include("component/sidebar.php");
  $user_id = $_SESSION['user_id'];
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Pendaftaran Mahasiswa Baru</h3>
                  </div>
                  <form role="form" method="POST" action="pendaftaran-baru.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">NIM</label>
                        <input disabled type="name" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id ?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Studi Program</label>
                        <select class="form-control" id="id_studi" name="id_studi">
                          <?php
                            $sql="SELECT 	
                              program_studi.studi_id AS STUDIID, 
                              program_studi.fakultas_id AS FAKULTASID,
                                program_studi.jurusan_id AS JURUSANID,
                                f.nama AS NAMAFAKULTAS,
                                j.nama AS NAMAJURUSAN
                            FROM program_studi
                            INNER JOIN jurusan j
                              ON j.jurusan_id = program_studi.jurusan_id
                            INNER JOIN fakultas f
                              ON f.fakultas_id = program_studi.fakultas_id";
                            $show_studiprogram = mysqli_query($connect, $sql);
                            while($row = mysqli_fetch_array($show_studiprogram)) {
                          ?>
                          <option value="<?php echo $row['STUDIID']; ?>" ><?php echo ''.$row['NAMAFAKULTAS'].' - '.$row['NAMAJURUSAN'].''; ?></option>
                           <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kategori</label>
                        <input disabled type="name" class="form-control" id="category" name="category" value="Mahasiswa Baru">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Biaya Pendaftaran</label>
                        <input disabled type="name" class="form-control" value="150,000">
                      </div>
                    </div>
                    <div class="box-footer">
                      <a href="index.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $id           = rand(1111,9999);
                      $user_id      =  $_SESSION['user_id'];
                      $id_studi     = $_POST['id_studi'];
                      $category     = 'Mahasiswa Baru';
                      $status       = 'Registrasi';
                      $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                      $modified_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                      $sql = "INSERT INTO regis_ppdb (id, user_id, id_studi, category, status, create_at, modified_at)
                              VALUES ('$id','$user_id','$id_studi','$category', '$status', '$create_at', '$modified_at')
                              ";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Anda berhasil mendaftar sebagai Mahasiswa Baru ');
                            window.location = 'index.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
