

<?php
  include("component/header.php");
  include("component/sidebar.php");
  include("function.php");
?>
  <div class="content-wrapper">
    <!-- Untuk menampilkan Pengumuman -->
    <section class="content-header">

    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Berkas Persyaratan Pendaftaran Mahasiswa</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                <tr>
                  <th>No</th>
                  <th>Berkas</th>
                  <th>Format</th>
                  <th>Nama Dokumen</th>
                  <th>Status Dokumen</th>
                  <th>Aksi</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Fotocopy Ijazah legalisir</td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $ijazah ?></td>
                  <td>
                    <?php 
                      if($ijazah !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                    <?php 
                      if($ijazah !== ''){
                        echo "<a href='dokumen-ijazah.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-ijazah.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Fotocopy SKHUN/NEM legalisir</td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $skhun ?></td>
                  <td>
                    <?php 
                      if($skhun !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                    <?php 
                      if($skhun !== ''){
                        echo "<a href='dokumen-skhun.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-skhun.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Fotocopy KTP</td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $ktp ?></td>
                  <td>
                    <?php 
                      if($ktp !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                  <?php 
                      if($ktp !== ''){
                        echo "<a href='dokumen-ktp.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-ktp.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Fotocopy KK</td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $kk ?></td>
                  <td>
                    <?php 
                      if($kk !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                    <?php 
                      if($kk !== ''){
                        echo "<a href='dokumen-kk.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-kk.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Foto Ukuran 2x3</td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $foto2x3 ?></td>
                  <td>
                    <?php 
                      if($foto2x3 !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                    <?php 
                      if($foto2x3 !== ''){
                        echo "<a href='dokumen-foto2x3.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-foto2x3.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>Foto Ukuran 3x4</td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $foto3x4 ?></td>
                  <td>
                    <?php 
                      if($foto3x4 !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                    <?php 
                      if($foto3x4 !== ''){
                        echo "<a href='dokumen-foto3x4.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-foto3x4.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>Transkrip Nilai <b>(Khusus Mahasiswa Pindahan/Lanjutan)</b></td>
                  <td>PDF (Maksimal 1 MB)</td>
                  <td><?php echo $transkrip ?></td>
                  <td>
                    <?php 
                      if($transkrip !== ''){
                        echo "<span style='color: green;'>Sudah terupload<span>";
                      } else {
                        echo "<span style='color: red;'>Belum diupload<span>";
                      } 
                    ?>
                  </td>
                  <td>
                    <?php 
                      if($transkrip !== ''){
                        echo "<a href='dokumen-transkrip.php'>Upload Ulang</a>";
                      } else {
                        echo "<a href='dokumen-transkrip.php'>Upload Dokumen</a>";
                      } 
                    ?>
                  </td>
                </tr>
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        

        

      </div>
      <div class="row">
        <div class="col-md-12">
          <?php
           if($skhun !== '' && $ijazah !== '' && $ktp !== '' && $kk !== '' && $foto2x3 !== '' && $foto3x4 !== '' && $transkrip !== ''){
                echo '
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Selamat!</h4>
                    Anda sudah dapat melakukan pendaftaran <b>Mahasiswa Baru</b>, <br/>upload <b>Transkrip Nilai</b> untuk dapat melakukan pendaftaran <b>Mahasiswa Lanjutan/Pindahan</b>.
                  </div>
                  <div class="col-md-3">
                    <a href="pendaftaran-baru.php" type="button" class="btn btn-block btn-info">Pendaftaran Mahasiswa Baru</a>
                    <a href="pendaftaran-lanjutan.php" type="button" class="btn btn-block btn-info">Pendaftaran Mahasiswa Lanjutan</a>
                    <a href="pendaftaran-pindahan.php" type="button" class="btn btn-block btn-info">Pendaftaran Mahasiswa Pindahan</a>
                  </div>
                ';
              } else if($skhun !== '' && $ijazah !== '' && $ktp !== '' && $kk !== '' && $foto2x3 !== '' && $foto3x4 !== '' ) {
                echo '
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Selamat!</h4>
                    Anda sudah dapat melakukan pendaftaran <b>Mahasiswa Baru</b>, <br/>upload <b>Transkrip Nilai</b> untuk dapat melakukan pendaftaran <b>Mahasiswa Lanjutan/Pindahan</b>.
                  </div>
                  <div class="col-md-3">
                    <a href="pendaftaran-baru.php" type="button" class="btn btn-block btn-primary">Pendaftaran Mahasiswa Baru</a>
                  </div>
                ';
              } else {
                echo '
                  <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-warning"></i> Tolong lengkapi berkas terlebih dahulu!</h4>
                    Mohon Maaf, Anda belum dapat melakukan pendaftaran, tolong lengkapi berkas terlebih dahulu
                  </div>
                ';
              }
          ?>
          
        </div>
      </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
  ?>
