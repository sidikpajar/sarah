
<?php
  include("component/header.php");
  include("component/sidebar.php");
  include("function.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Upload SKHUN/NEM</h3>
                  </div>
                  <form role="form" method="POST" action="dokumen-skhun.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Upload File SKHUN</label>
                        <input type="file" class="form-control" id="skhun" name="skhun" required>
                      </div>
                    </div>
                    <div class="box-footer">
                      <a href="index.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])){
                        $check = $_FILES["skhun"]["tmp_name"];
                        if($check !== false){
                            $random_number  = rand(111111,999999);
                            $user_id         = $_SESSION['user_id'];
                            $lokasi_file     = $_FILES['skhun']['tmp_name'];
                            $nama_file       = $_FILES['skhun']['name'];
                            $folder         = "../files/$user_id+skhun.pdf";

                            // menghapus file sebelumnya
                            $files    = glob("../files/{$skhun}");
                            foreach ($files as $file) {
                              if (is_file($file))
                              unlink($file); // hapus file sebelumnya
                            }

                            if (move_uploaded_file($lokasi_file,"$folder")){
                              echo "Nama File : <b>$nama_file</b> sukses di upload";

                              if($user_id_berkas !== $user_id){
                                $sql = "INSERT INTO berkas (id, user_id, ijazah, skhun, ktp, kk, foto2x3, foto3x4, transkrip) 
                                VALUES ('$random_number','$user_id','$ijazah','$user_id+skhun.pdf','$ktp','$kk','$foto2x3','$foto3x4','$transkrip')";
                              }

                              if($user_id_berkas === $user_id){
                                $sql = "UPDATE berkas SET
                                skhun='$user_id+skhun.pdf'
                                WHERE user_id = '$user_id' ";
                              }
 
                              if ($connect-> query($sql) === TRUE) {
                                  echo "
                                  <script type= 'text/javascript'>
                                      alert('berhasil memperbaharui SKHUN');
                                      window.location = 'index.php';
                                  </script>";
                                  } else {
                                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                      }
                            }
                            else{
                              echo "File gagal di upload";
                            }

                        }else{
                            echo "Please select an pdf file to upload.";
                        }
                    }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
