
<?php
session_start();

//cek apakah user sudah login
if(!isset($_SESSION['user_id'])){
  print "Anda belum login, Silakan ke menu login";
  print "<br/><a href='/pendaftaran/login'>login</a><br/>";
  die();
}

//cek level user
if($_SESSION['user_level']!="mahasiswa")
{
    die("Anda bukan Mahasiswa");
}
?>
