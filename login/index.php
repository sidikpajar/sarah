<?php
session_start();
if($_SESSION){
    if($_SESSION['user_level']=="admin")
    {
        header("Location: admin/index.php");
    }
    if($_SESSION['user_level']=="mahasiswa")
    {
        header("Location: mahasiswa/index.php");
    }
}

include('login.php');
include('../components/header.php'); 
?>
    <div class="container" style="margin-top:50px">
      <h1>Login Calon Mahasiswa</h1>
      <form method="post" action="">
        <div class="form-group">
          <label>Username</label>
          <input type="text" name="user_name" class="form-control"  placeholder="hellofajars">
          <small id="emailHelp" class="form-text text-muted">Digunakan untuk melakukan login kedalam aplikasi</small>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="text" name="user_password" class="form-control"  placeholder="********">
        </div>
        <button type="submit" name="submit" value="Login" class="btn btn-success pull-right">Submit</button>
      </form>
      
    </div>
<?php include('../components/footer.php'); ?>

