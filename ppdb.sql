-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 22, 2020 at 09:10 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `berkas`
--

CREATE TABLE `berkas` (
  `id` varchar(33) NOT NULL,
  `user_id` varchar(33) NOT NULL,
  `ijazah` text NOT NULL,
  `skhun` text NOT NULL,
  `ktp` text NOT NULL,
  `kk` text NOT NULL,
  `foto2x3` text NOT NULL,
  `foto3x4` text NOT NULL,
  `transkrip` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berkas`
--

INSERT INTO `berkas` (`id`, `user_id`, `ijazah`, `skhun`, `ktp`, `kk`, `foto2x3`, `foto3x4`, `transkrip`) VALUES
('654868', '2167410301', '2167410301+ijasah.pdf', '2167410301+skhun.pdf', '2167410301+ktp.pdf', '2167410301+kk.pdf', '2167410301+foto2x3.pdf', '2167410301+foto3x4.pdf', '2167410301+transkrip.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `fakultas_id` varchar(33) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`fakultas_id`, `nama`) VALUES
('4056291856', 'FAKULTAS INDUSTRI KREATIF'),
('9302310133', 'FAKULTAS BISNIS');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `jurusan_id` varchar(33) NOT NULL,
  `nama` text NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`jurusan_id`, `nama`, `deskripsi`) VALUES
('0279575429', 'Akuntansi (S1)', 'Akuntansi mempersiapkan mahasiswa untuk mempunyai kemampuan dalam menerapkan ilmu akuntansi berbasis penggunaan ICT (Information Communication Technology) untuk tata kelola yang baik bagi industri berbasis jasa dan sektor publik, dengan kekhasan industri life science.'),
('2882274653', 'Magister Manajemen (S2)', '<div class=\"fr-view\"><div class=\"fr-view\"><div class=\"fr-view\"><div class=\"fr-view\"><div class=\"fr-view\"><div class=\"fr-view\"><div class=\"sub-juduls\"><p>MM KALBIS Institute merupakan program studi yang mengkaji, menerapkan dan mengembangkan Ilmu Manajemen, yang meliputi perencanaan dan pengelolaan strategic untuk membekali para lulusan menjadi pelaku bisnis yang mampu melakukan perencanaan dan pengelolaan bisnis melalui penerapan konsep dan strategi yang tepat serta mampu mengantisipasi perubahan-perubahan di masa mendatang.</p><p>&nbsp;</p><p><strong>Keunggulan MM Strategic Management Kalbis Institute :</strong></p><p><strong>Real Case</strong></p><p>Membantu pengembangan diri mahasiswa, MM Kalbis menggunakan kasus-kasus bisnis nyata untuk menjelaskan teori dan penerapannya dalam praktik bisnis sesungguhnya.</p><p><strong>Student Centered Learning</strong></p><p>Sistem ini memberikan kesempatan bagi mahasiswa untuk lebih banyak mengemukakan pendapat dan saling berinteraksi sehingga setiap mahasiswa dapat berkontrbusi secara aktif untuk memahami materi.</p><p><strong>Studium Generale / Kuliah Umum</strong></p><p>MM Kalbis juga menyelenggarakan kuliah umum secara regular khususnya untuk mahasiswa aktif dengan nama studium generale. Untuk membahas topik-topik aktual bagi mahasiswa dan mengundang narasumber kompeten praktisi maupun akademisi yang akan menambah wawasan mahasiswa mengenai kurikulum manajemen.</p><p><strong>Kurikulum</strong></p><p>Kurikulum program studi Magister Manajemen Kalbis Institute dirancang dalam rangka membentuk profil lulusan MM yang memiliki kompetensi unggul dalam berbagai disiplin ilmu manajemen terapan seperti Manajemen Pemasaran, Manajemen Keuangan, Manajemen Sumber Daya Manusia, Manajemen Project &amp; Operasional, Perilaku Organisasi, Komunikasi Pemasaran Terpadu, Manajemen Inovasi, Manajemen CSR, Strategi Kompetitif, Riset Pemasaran, Pengambil Keputusan dan Perialku Konsumen, Lulusan MM Kalbis memiliki wawasan keilmuan manajemen terapan yang dapat memberikan kontribusi positif dalam perusahaan baik sebagai manajer/ staf senior/pimpinan perusahaan maupun berwirausaha.&nbsp;</p></div></div></div></div></div></div></div>'),
('6861767523', 'Manajemen (S1)', 'Manajemen mempersiapkan mahasiswa agar mampu menerapkan ilmu pengetahuan dan keterampilan di bidang manajemen yang berorientasi pada peningkatan nilai tambah (value based oriented) berbasis ICT (Information Communication Technology), dengan kekhasan industri life science.'),
('9941862716', 'Business in Creative Industry (S1)', 'Program Business in Creative Industry mempersiapkan mahasiswa untuk mempunyai kemampuan memahami dinamika bisnis industri kreatif, mampu merancang rencana bisnis untuk usaha mandiri, mampu merancang strategi pemasaran terpadu, memanfaatkan teknologi berbasis internet untuk bersaing, mewujudkan ide-ide kreatif yang memiliki nilai komersial serta mampu mengelola kegiatan-kegiatan kreatif dalam organisasi');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` varchar(33) NOT NULL,
  `user_id` varchar(33) NOT NULL,
  `registrasi_id` varchar(33) NOT NULL,
  `tanggal_transfer` date NOT NULL,
  `atas_nama` varchar(100) NOT NULL,
  `nomor_rek` varchar(100) NOT NULL,
  `nominal` bigint(10) NOT NULL,
  `image` text NOT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `user_id`, `registrasi_id`, `tanggal_transfer`, `atas_nama`, `nomor_rek`, `nominal`, `image`, `create_at`, `modified_at`) VALUES
('86904', '2167410301', '5252', '2020-06-27', 'Aditya Putra', '33531323', 150000, '86904+photo_2020-06-18 10.48.54.jpeg', '2020-06-20 19:22:13', '2020-06-20 19:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `program_studi`
--

CREATE TABLE `program_studi` (
  `studi_id` varchar(33) NOT NULL,
  `fakultas_id` varchar(33) NOT NULL,
  `jurusan_id` varchar(33) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_studi`
--

INSERT INTO `program_studi` (`studi_id`, `fakultas_id`, `jurusan_id`) VALUES
('3647071852', '9302310133', '6861767523'),
('4996569318', '9302310133', '9941862716'),
('5703198622', '9302310133', '0279575429'),
('62363737', '4056291856', '9941862716'),
('8868177337', '9302310133', '2882274653');

-- --------------------------------------------------------

--
-- Table structure for table `regis_ppdb`
--

CREATE TABLE `regis_ppdb` (
  `id` varchar(33) NOT NULL,
  `user_id` varchar(33) NOT NULL,
  `id_studi` varchar(33) NOT NULL,
  `category` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regis_ppdb`
--

INSERT INTO `regis_ppdb` (`id`, `user_id`, `id_studi`, `category`, `status`, `create_at`, `modified_at`) VALUES
('5252', '2167410301', '4996569318', 'Mahasiswa Lanjutan', 'Diterima', '2020-06-19 17:53:55', '2020-06-19 17:53:55'),
('6254', '2167410301', '3647071852', 'Mahasiswa Baru', 'Ditolak', '2020-06-19 16:55:50', '2020-06-21 14:50:30'),
('9624', '2167410301', '8868177337', 'Mahasiswa Pindahan', 'Registrasi', '2020-06-19 17:54:01', '2020-06-19 17:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(33) NOT NULL,
  `user_level` varchar(10) NOT NULL,
  `user_password` text NOT NULL,
  `user_name` text NOT NULL,
  `name` text NOT NULL,
  `gender` varchar(100) NOT NULL,
  `maritial_status` varchar(100) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `birth_date` date NOT NULL,
  `place_of_birth` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_level`, `user_password`, `user_name`, `name`, `gender`, `maritial_status`, `religion`, `birth_date`, `place_of_birth`, `status`) VALUES
('2167410301', 'mahasiswa', 'helloadit', 'helloadit', 'Aditya Putra', 'Pria', 'Belum Kawin', 'Islam', '2020-06-01', 'Jakarta', 'ACTIVE'),
('4746770762', 'admin', 'admin', 'admin', 'admin', 'Perempuan', '', '', '2020-06-01', '', 'ACTIVE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berkas`
--
ALTER TABLE `berkas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`fakultas_id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`jurusan_id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`studi_id`);

--
-- Indexes for table `regis_ppdb`
--
ALTER TABLE `regis_ppdb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
